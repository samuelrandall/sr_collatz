#!/usr/bin/env python3

# ---------------------------
# projects/collatz/SphereCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

import sys

# instantiate lazy cache - no predetermined values for sake of brevity
# must to prefill values in cache(list) to be overwritten during eval
global cache
cache = ([0] * 1000001)

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------

def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert i > 0 and i < 1000000
    assert j > 0 and i < 1000000

    max = 0
    # must prefill this value
    cache[1] = 1

    if j < i:
        i, j = j, i
    # this assertion must come after above if statement
    assert i <= j

    for n in range(i, j + 1):
        count = 0
        current = n

        # check to see if present in cache
        if cache[n] != 0:
            count = cache[n]

        # if not present in cache
        else:
            while n > 1:
                if (n % 2) == 0:
                    n >>= 1
                    count += 1
                else:
                    n += (n >> 1) + 1
                    count += 2

                # if within bounds and not present in cache already, assign to
                # count
                if n <= 1000000 and cache[n] != 0:
                    count += cache[n]
                    break

        cache[current] = count
        if count > max:
            max = count

    assert max > 0
    return max

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        if not s.strip():
            continue
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)


if __name__ == "__main__":
    collatz_solve(sys.stdin, sys.stdout)

""" #pragma: no cover
% cat RunCollatz.in
1 10
100 200
201 210
900 1000



% RunCollatz.py < RunCollatz.in > RunCollatz.out



% cat RunCollatz.out
1 10 1
100 200 1
201 210 1
900 1000 1



% pydoc3 -w Collatz
# That creates the file Collatz.html
"""
